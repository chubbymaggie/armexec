set print pretty on

# view registers
define arm_r
    set $i = 0
    while ($i < 16)
        set $r = __exec_env.core_registers[$i]
        printf "r%d:\t0x%x\t%d\n", $i++, $r, $r
    end
    printf "cpsr:\tN:%d Z:%d C:%d V:%d\n", __exec_env.cpsr[0], __exec_env.cpsr[1], __exec_env.cpsr[2], __exec_env.cpsr[3]
end
document arm_r
Print all ARM registers
end

# view backtrace
define arm_bt
    set $i = 0
    set $d = __exec_env.fridx
    while ($i < $d)
        set $lr =  __exec_env.frames[$i].lr
        set $entry = __exec_env.frames[$i].entry
        set $name = __exec_env.frames[$i].name
        printf "#%d\t%x\t[%s] : %x\t\n", $i++, $lr, $name, $entry
    end
end
document arm_bt
Print backtrace
end

# break on addr
define arm_b
set exec_breakpoint = $arg0
end
document arm_b
Break on addr
end

# remove breakpoint
define arm_clear
set exec_breakpoint = 0xffffffff
end
document arm_clear
Remove breakpoint
end

# enter/quit step mode
define arm_step
    if ($arg0 == 1)
        set exec_breakpoint = 0x0
    else
        set exec_breakpoint = 0xffffffff
    end
end
document arm_step
Enter/Quit step mode
end
