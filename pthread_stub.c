/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>

#include "armexec.h"
#include "plt_stub.h"


/* thread id starts from 1.
 * thread id 1 is reserved for main thread.
 */

#define MAIN_THREAD_ID  1

static volatile int thread_curr_index = MAIN_THREAD_ID;

__thread int thread_id = MAIN_THREAD_ID;

#define __MUTEX_MAX     256
static volatile int mutex_curr_index = 0;
static pthread_mutex_t mutexes[__MUTEX_MAX];

#define _SEM_MAX        __MUTEX_MAX
static volatile int sem_curr_index = 0;
static sem_t sems[__MUTEX_MAX];

struct thread_arg {
    int         id;
    uint32_t    start;
    uint32_t    halt;
    uint32_t    arg;
    struct armld *ld;
};

#define __PTHREAD_MAX   64

static uint32_t thread_errnos[__PTHREAD_MAX];

/* The slot 0 and 1 of the two arrays are wasted */
static struct thread_arg thread_args[__PTHREAD_MAX];
static pthread_t threads[__PTHREAD_MAX];

DCLR_STUB(__errno)
{
    struct vm *vm = ld->vm;
    uint32_t data = thread_errnos[thread_id];

    if (!data)
        data = vm_malloc(vm, 4, NULL);

    SW(data, errno);

    return data;
}

static void *
thread_entry(void *arg)
{
    struct thread_arg *a = arg;
    uint32_t regs[4] = {0};

    thread_id = a->id;
    regs[0] = a->arg;
    arm_exec(a->ld, a->start, a->halt, regs, 4);

    return NULL;
}

DCLR_STUB(pthread_create)
{
    struct vm *vm = ld->vm;
    struct thread_arg *arg;
    int id;
    pthread_t *th;
    uint32_t data;

    /* To translate Bionic attr to local attr
     * is boring. We ignore all but one property
     * of Bionic attr: detach state.
     * The Bionic attr is as below:
     *
     * See https://github.com/android/platform_bionic/blob/master/libc/include/machine/pthread_types.h
     *
     * typedef struct {
     *     uint32_t flags;
     *     void* stack_base;
     *     size_t stack_size;
     *     size_t guard_size;
     *     int32_t sched_policy;
     *     int32_t sched_priority;
     *   #ifdef __LP64__
     *     char __reserved[16];
     *   #endif
     * } pthread_attr_t;
     *
     * Among which, flags defines the detach state.
     * See https://github.com/android/platform_bionic/blob/master/libc/include/pthread.h
     *
     * #define PTHREAD_CREATE_DETACHED  0x00000001
     *
     * Therefore, we check only the LSB of flags.
     */
    pthread_attr_t attr;
    uint32_t flags;

    pthread_attr_init(&attr);

    if (r1) {
        flags = LW(r1);
        if (flags & 0x1)
            pthread_attr_setdetachstate(&attr, 1);
    }

    id = __sync_add_and_fetch(&thread_curr_index, 1);
    th = &threads[id];

    arg = &thread_args[id];
    arg->id = id;
    r2 &= ~1;
    arg->start = r2;
    arg->halt = 0;
    arg->arg = r3;
    arg->ld = ld;
    SW(r0, id);

    return pthread_create(th, &attr, thread_entry, arg);
}

DCLR_STUB(pthread_attr_setdetachstate)
{
    struct vm *vm = ld->vm;
    uint32_t *flags = PA(r0);

    if (r1)
        *flags |= 1;
    else
        *flags &= ~1;

    return 0;
}

DCLR_STUB(pthread_self)
{
    return thread_id;
}

DCLR_STUB(pthread_detach)
{
    if (thread_id == MAIN_THREAD_ID)
        abort();
    return pthread_detach(threads[thread_id]);
}

DCLR_STUB(pthread_join)
{
    if (r0 == MAIN_THREAD_ID)
        abort();
    return pthread_join(threads[r0], NULL);
}

DCLR_STUB(pthread_exit)
{
    pthread_exit(NULL);
    return 0;
}

DCLR_STUB(pthread_mutex_init)
{
    struct vm *vm = ld->vm;
    int id;
    pthread_mutex_t *m;
    int ret;

    id = __sync_fetch_and_add(&mutex_curr_index, 1);
    m = &mutexes[id];

    ret = pthread_mutex_init(m, NULL);
    SW(r0, id);

    return ret;
}

DCLR_STUB(pthread_mutex_destroy)
{
    struct vm *vm = ld->vm;
    uint32_t mid = LW(r0);
    pthread_mutex_t *m = &mutexes[mid];

    return pthread_mutex_destroy(m);
}

DCLR_STUB(pthread_mutex_lock)
{
    struct vm *vm = ld->vm;
    uint32_t mid = LW(r0);
    pthread_mutex_t *m = &mutexes[mid];

    return pthread_mutex_lock(m);
}

DCLR_STUB(pthread_mutex_unlock)
{
    struct vm *vm = ld->vm;
    uint32_t mid = LW(r0);
    pthread_mutex_t *m = &mutexes[mid];

    return pthread_mutex_unlock(m);
}

DCLR_STUB(sem_init)
{
    struct vm *vm = ld->vm;
    int id;
    sem_t *s;
    int ret;

    id = __sync_fetch_and_add(&sem_curr_index, 1);
    s = &sems[id];

    ret = sem_init(s, r1, r2);
    SW(r0, id);

    return ret;
}

DCLR_STUB(sem_destroy)
{
    struct vm *vm = ld->vm;
    sem_t *s;
    uint32_t sid = LW(r0);

    s = &sems[sid];

    return sem_destroy(s);
}

DCLR_STUB(sem_wait)
{
    struct vm *vm = ld->vm;
    sem_t *s;
    uint32_t sid = LW(r0);

    s = &sems[sid];

    return sem_wait(s);
}

DCLR_STUB(sem_post)
{
    struct vm *vm = ld->vm;
    sem_t *s;
    uint32_t sid = LW(r0);

    s = &sems[sid];

    return sem_post(s);
}

DCLR_STUB(sem_getvalue)
{
    struct vm *vm = ld->vm;
    sem_t *s;
    uint32_t sid = LW(r0);
    int *sval = PA(r1);

    s = &sems[sid];

    return sem_getvalue(s, sval);
}

DCLR_STUB(sched_yield)
{
    return sched_yield();
}
