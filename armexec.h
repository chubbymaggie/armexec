/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _ARM_EXEC_H
#define _ARM_EXEC_H

#include <stdint.h>
#include <stdio.h>

struct vm;
struct elf;

struct armld {
    struct vm       *vm;
    struct elf      *elf;
};

/* Defined in pthread_stub.c */
extern __thread int thread_id;

/* Defined in exec.c */
extern __thread uint32_t exec_curr_insn;

#define __breakpoint() do { \
    __asm__ volatile("int $0x03"); \
} while(0)

#include "log.h"
#include "vm.h"
#include "elf_ld.h"
#include "exec.h"

#endif
